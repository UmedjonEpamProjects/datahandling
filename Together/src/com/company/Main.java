package com.company;

import com.Data.DateService;
import com.Data.DateServiceImpl;
import com.Others.OtherService;
import com.Others.OtherServiceImpl;
import com.Strings.StringService;
import com.Strings.StringServiceImpl;

import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws ParseException {
        StringService stringService = new StringServiceImpl();
        OtherService otherService = new OtherServiceImpl();
        DateService dateService = new DateServiceImpl();

        System.out.println("Method №1");
        stringService.transfer();
        System.out.println("Method №2");
        stringService.word();
        System.out.println("Method №3");
        stringService.number();
        System.out.println("Method №4");
        stringService.replaceTemplate();

        System.out.println("-------------------------");

        System.out.println("Method №1");
        otherService.area();
        System.out.println("Method №2");
        otherService.threeNumbers();

        System.out.println("-------------------------");

        System.out.println("Method 1");
        dateService.ageBirthday();
        System.out.println("Method 2");
        dateService.diffday();
        System.out.println("Method 3");
        dateService.ToConvert();



    }
}
