package com.Strings;

public class StringServiceImpl implements StringService {

    @Override
    public void transfer() {
        String str = "НЕВОЗМОЖНОСТЬ слово из словаря глупцов. Наполеон Бонапарт";
        for (String word: str.split(" ")) {
            System.out.println(word.toLowerCase());
        }
    }

    @Override
    public void word() {
        String str = "НЕВОЗМОЖНОСТЬ слово из словаря глупцов. Наполеон Бонапарт";
        String[]words = str.split(" ");
        String resstring = "";

        for(String word: words) {
            if(word.length() > resstring.length())
                resstring=word;
        }
        System.out.println("MaxWordLengthString "+"*"+resstring+"*");

        for(String word: words) {
            if(word.length() < resstring.length())
                resstring = word;
        }
        System.out.println("MinWordLengthString "+"*"+resstring+"*");

    }

    @Override
    public void number() {
        String[] list = {"+7 (3412) 517-647", "8 (3412) 4997-12","+7 (3412) 90-41-90"};

        for (int i = 0; i<list.length; i++ ){

            if (list[0] == "+7 (3412) 517-647") {
                String result = list[i].replaceAll("(\\d{3})-(\\d{3})", "");
                String phoneNumber = list[i].replace(result, "");
                System.out.print(phoneNumber);
            }
            if (list[1] == "8 (3412) 4997-12") {
                String result = list[i].replaceAll("(\\d{4})-(\\d{2})", "");
                String phoneNumber = list[i].replace(result, "");
                System.out.print(phoneNumber);
            }
            if (list[2] == "+7 (3412) 90-41-90") {
                String result = list[i].replaceAll("(\\d{2})-(\\d{2})-(\\d{2})", "");
                String phoneNumber = list[i].replace(result, "");
                System.out.print(phoneNumber);
            }

            System.out.println();
        }

    }

    @Override
    public void replaceTemplate() {
        String[] name = {"Давлатов Умеджон", "Теле 2"};
        int[] account = {5, 123};

        String str = "Уважаемый," + name[0] + ", ваш баланс менее " +account[0]+ "р. Используйте услугой обещанный платеж, наберите. " +account[1]+ " С уважением, " + name[1];
        System.out.println(str);
        System.out.println("Так же дана пара строк");

        String templateKey = "Крешитиану Роналдо 5 кратный облодатель золотого мяча";
        String templateValue = "Леонель Месси";
        String result = templateKey.replace("Крешитиану Роналдо" , templateValue);
        System.out.println(result);

    }
}
