package com.Data;

import java.text.ParseException;

public interface DateService {
    void ageBirthday() throws ParseException;
    void diffday() throws ParseException;
    void ToConvert();
}
