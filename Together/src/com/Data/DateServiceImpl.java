package com.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateServiceImpl implements DateService {

    @Override
    public void ageBirthday() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date1 = dateFormat.parse("27.08.1994");
        Date date2 = dateFormat.parse("25.12.2017");

        long milliseconds = date2.getTime() - date1.getTime();

        int seconds = (int) (milliseconds / (1000));
        System.out.println("ваш возраст с момента рождения в секундах: " + seconds);

        // 60 000 миллисекунд = 60 секунд = 1 минута
        int minutes = (int) (milliseconds / (60 * 1000));
        System.out.println("ваш возраст с момента рождения в минутах: " + minutes);

        // 3 600 секунд = 60 минут = 1 час
        int hours = (int) (milliseconds / (60 * 60 * 1000));
        System.out.println("ваш возраст с момента рождения в часах: " + hours);

        int days = (int) (milliseconds / (24 * 60 * 60 * 1000));
        System.out.println("ваш возраст с момента рождения в днях: " + days);

        int year = (days / 365);
        System.out.println("ваш возраст с момента рождения в годах: " + year);

    }

    @Override
    public void diffday() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date1 = dateFormat.parse("27.08.1994");
        Date date2 = dateFormat.parse("22.12.2017");
        System.out.println("Первая дата: " + date1 + " Вторая дата: " +date2);
        long milliseconds = date2.getTime() - date1.getTime();
        // 1000 миллисекунд = 1 секунда
        // 24 часа = 1 440 минут = 1 день
        int days = (int) (milliseconds / (24 * 60 * 60 * 1000));
        System.out.println("Разница между датами в днях: " + days);

    }

    @Override
    public void ToConvert() {
        String line = "24 February 2018";
        LocalDate date = LocalDate.parse(line, DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.US));
        System.out.println(date.format(DateTimeFormatter.ofPattern("dd/MMM/yy" , new Locale("US"))));

    }
}
