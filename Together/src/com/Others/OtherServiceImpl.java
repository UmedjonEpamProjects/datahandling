package com.Others;

public class OtherServiceImpl implements OtherService {

    final double PI = 3.14;

    @Override
    //площадь круга
    public void area() {
        double r = 4; //радиус
        System.out.println("Площадь круга S = " + PI * Math.pow(r,2));

    }
    //сумма трех чисел
    @Override
    public void threeNumbers() {
        String a = "0.1", b = "1.5", sum = "0.25";
        double x = Double.parseDouble(a);
        double y = Double.parseDouble(b);
        double z = Double.parseDouble(sum);

        if ((x + y) == z) {
            System.out.println("Сумма двух чисел = третьему ");
        }else {
            System.out.println("Сумма двух чисел != третьему ");
        }
    }
}
