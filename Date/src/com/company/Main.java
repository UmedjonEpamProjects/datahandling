package com.company;

import com.DateImpl.DateImplement;

import java.lang.reflect.Method;
import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws ParseException {
        DateImplement dateImplement = new DateImplement();

        System.out.println("Method 1");
        dateImplement.bday();
        System.out.println("Method 2");
        try {
            dateImplement.diffday();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Method 3");
        dateImplement.daymonthyear();
    }
}
